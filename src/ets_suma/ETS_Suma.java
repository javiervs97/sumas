package ets_suma;

import java.util.Scanner;

public class ETS_Suma {

    public static void main(String[] args) {

        int n = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduza la N: ");
        n = sc.nextInt();
        suma(n);

    }

    static void suma(int n) {

        double x;
        
        x = (n + Math.pow(n, 2))/2;
        
        System.out.println("La suma de los " + n + " primeros números es: " + x);

    }

}
